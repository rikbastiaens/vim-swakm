"Function definitions
function! UnderlineHeading()
  normal! yypVr-
endfunction 

function! StyleOptions(style)
  if a:style == 'b'
    exe "normal! c****\<esc>h\"P"
  elseif a:style == 'bi'
    exe "normal! c**__**\<esc>hhP"
  elseif a:style == 'i'
    exe "normal! c**\<esc>P"
  elseif a:style == 's'
    exe "normal! c~~\<esc>P"
  else 
    echom 'No valid style option selected.'
  endif
endfunction

function! Insert(type)
  let curline = getline('.')
  call inputsave()
  let url = input('Enter url: ')
  call inputrestore()
  call inputsave()
  let alt = input('Enter alt text: ')
  call inputrestore()
  if a:type == 'link'
    call setline('.', curline . '['  . alt . '](' . url . ')')
  elseif a:type == 'img'
    call setline('.', curline . '!['  . alt . '](' . url . ')')
  endif
endfunction

function! MakeList(type)
  if a:type == 'numbered'
    echom 'Not implemented yet.'
  elseif a:type == 'unnumbered'
    normal! 0I* 
    stopinsert
  else 
    echom 'Not a known list type.'
  endif
endfunction

function! MakeNumberedList()
  let @n = 1
  let curline = getline('.')
  for l:line in getline(line("'<"), line("'>"))
    echom @n
    call setline(@n, @n . '. ' . l:line)
    let @n = @n + 1
    continue
  endfor
endfunction

function! CodeBlock()
  exe "normal! c```\<cr>```\<esc>0P"
endfunction

function! SetHeader(level)
  if a:level == 1
    exe "normal! 0c$# \<esc>p"
  elseif a:level == 2
    exe "normal! 0c$## \<esc>p"
  elseif a:level == 3
    exe "normal! 0c$### \<esc>p"
  else 
    echom "Level not specified."
  endif
endfunction

function! UnmapSwakm()
  vnoremap <localleader>i <nop>
  vnoremap <localleader>b <nop>
  vnoremap <localleader>bi <nop>
  vnoremap <localleader>s <nop>
  nnoremap <localleader>img <nop>
  nnoremap <localleader>link <nop>
  nnoremap <localleader>h <nop>
  nnoremap <localleader>hh <nop>
  nnoremap <localleader>hhh <nop>
  nnoremap <localleader>ul <nop>
  vnoremap <localleader>ul <nop>
  nnoremap <localleader>u <nop>
  vnoremap <localleader>c <nop>
endfunction

" make current word italic
autocmd FileType markdown vnoremap <localleader>i :call StyleOptions('i')
" make current word bold
autocmd FileType markdown vnoremap <localleader>b :call StyleOptions('b')
" make current word bold italic
autocmd FileType markdown vnoremap <localleader>bi :call StyleOptions('bi')
" strike current word through
autocmd FileType markdown vnoremap <localleader>s :call StyleOptions('s')

" insert commands
autocmd FileType markdown nnoremap <localleader>img :call Insert('img')
autocmd FileType markdown nnoremap <localleader>link :call Insert('link')

" Header commands
autocmd FileType markdown nnoremap <localleader>h :call SetHeader(1)
autocmd FileType markdown nnoremap <localleader>hh :call SetHeader(2)
autocmd FileType markdown nnoremap <localleader>hhh :call SetHeader(3)

" create bullet from single line or selection of lines
autocmd FileType markdown vnoremap <localleader>ul :call MakeList('unnumbered')
autocmd FileType markdown nnoremap <localleader>ul :call MakeList('unnumbered')

" Bind Underline command to leader u
" autocmd FileType markdown nnoremap <localleader>u :call UnderlineHeading()

" Create a codeblock and enter it
autocmd FileType markdown vnoremap <localleader>c :call CodeBlock()
