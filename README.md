# SWAKM: Swiss Army Knife for Markdown

## What is the goal of this tool?

This tool is meant for frequent users of Markdown as a syntax language for creating documentation.  
The plan is to create functions and bindings for common actions such as underlining, setting the text to bold, et cetera.

## Installation 

Installation can be done by using a plugin manager like `vundle` or `pathogen`, or simply sourcing the `swakm.vim` file in your vimrc.
